# Installation

* Enable module menu_perms_per_menu
* Go to `admin/people/permissions`. For each menu you should be provided with these permissions:
  * `Add new links to [menu name] menu from the menu interface`:
  * `Delete links in [menu name] menu from the menu interface`
  * `Edit link of menu links in [menu name] menu`
  * `Enable/disable links in [menu name] menu`
  * `Expand links in [menu name] menu`
  * `Translate links in [menu name] menu from the menu interface`
